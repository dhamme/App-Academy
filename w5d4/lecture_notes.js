var myHorse = new Horse();

Horse.prototype.goToTheMall = function(thingToDoWhenIGetThere){
	//goes to the mall
	thingToDoWhenIGetThere();
}
var callMe = function() { call(jeff) };
myHorse.goToTheMall(); 
// a callback is an arguement that is passed to a function with th intention of 
// it being executed later. aka a handler is a function

// When passing arguments as callbacks, do not add (), as this actually call
// the method. Instead pass the function's name without parenthesis

var cb = function(content){ console.log(content) };
File.read("my_text.txt", cb);

// this illustrates javascripts's asynchronicity. 

if(skyIsBlue){
	console.log("stuff");
} else {
	console.log("other stuff");
}

//false things
"", [], NaN, undefined, null, false

horse = new Horse() //global variable
var horse = new Horse() //local variable