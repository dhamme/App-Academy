function quicksort(ary) {
	if(ary.length < 2){
		return ary;
	}

	var pivot = ary[0];
	var rest = ary.slice(1)
	var left = [], right = [];

	var eachNumCallBack = function(num){
		if(num < pivot){
			left.push(num);
		} else {
			right.push(num);
		}
	};
	var result = quicksort(left).concat([pivot]).concat(quicksort(right));
	rest.forEach(eachNumCallBack);
}