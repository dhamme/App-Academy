// Modules
(function(myRootObject){

var MyModule = myRootObject.MyModule = myRootObject.MyModule || {};

MyModule.lunch = function() {
	// 'call' specifies what 'this' will refer to inside the 'slice' function
	// this also works: Array.prototype.slice.bind(arguments)();
	var foods = Array.prototype.slice.call(arguments);
	announce("I am going to eat " + foods.join(", "));
}

// private function in the local scope
function announce(string){
	alert(string);
}
// since this is a module, 'this' will usually be 'window'
})(this);

// prototypes
function Dog(name){
	this.name = name;
}

var jeff = new Dog("jeff");
// when creating a new object constructor style, 
// 1. New instance of object is instantiated 
// 2. Sets 'this' to our new object
// 3. Sets __proto__ on new object equal to parent class' prototype object
// 4. returns object

Dog.prototype.bark = 
	function() {
		console.log('bark bark, I\'m ' + this.name)
	};

function Surrogate(){};
Surrogate.prototype = Dog.prototype;
BadDog.prototype = newe Surrogate();

// instead of 32-34, we could also do
//
// want BadDog.__proto__ === Dog.prototype

BadDog.prototype.peeOnFloor = function(){ console.log('sssss :)')}