var sum = function () {
  var args = Array.prototype.slice.call(arguments);
  var total = 0;
  args.forEach(function(el){
    total += el;
  })
  return total;
}

// console.log(sum(1,2,3,45));

Function.prototype.myBind = function (object) {
  argArray = Array.prototype.slice.call(arguments);
  argArray.shift();
  
  var that = this;
  return function(){
    innerArgArray = Array.prototype.slice.call(arguments);
    argArray = argArray.concat(innerArgArray);
    return that.apply(object, argArray);
    // return undefined;
  }
  // return undefined;
};


var curriedSum = function(numArgs) {
  var numbers = [];
  var _curriedSum = function(num) {
    numbers.push(num);
    if (numbers.length === numArgs) {
      numbers.reduce(function(a, b){
        return a + b;
      })
    } else {
      return _curriedSum;
    }
  }
  return _curriedSum;
}

var sum = curriedSum(4);
sum(3)(4)(5)(2)


Function.prototype.curry = function(numArgs) {
  var args = [];
  
  var that = this;
  var _curry = function(arg) {
    args.push(arg);
    if (args.length === numArgs) {
      that(args);
    } else {
      return _curry;
    }
  }
  return _curry;
}


Function.prototype.inherits = function(SuperClass) {
  function Surrogate() {};
  Surrogate.prototype = SuperClass.prototype;
  this.prototype = new Surrogate();
}