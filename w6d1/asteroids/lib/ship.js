(function(){
	var Ship = Asteroids.Ship = function(pos, game) {
		var RADIUS = 10;
		var COLOR = '#0000ff';
		var VEL = [0,0];
		var attributes = {
			color: COLOR,
      radius: RADIUS,
      pos: pos,
      vel: VEL,
      game: game,
      isWrappable: true
		};
		Asteroids.MovingObject.call(this, attributes);
	};
	Asteroids.Util.inherits(Asteroids.Ship, Asteroids.MovingObject);

	Asteroids.Ship.prototype.relocate = function() {
		this.pos = this.game.randomPosition();
		this.vel = [0,0];
	}

	Asteroids.Ship.prototype.power = function(impulse) {
		var speedLimit = 10;
		if(Math.abs(this.vel[0] + impulse[0]) <= speedLimit){
			this.vel[0] += impulse[0];
		}
		if(Math.abs(this.vel[1] + impulse[1]) <= speedLimit){
			this.vel[1] += impulse[1];
		}
		console.log(this.vel);
	}

	Asteroids.Ship.prototype.fireBullet = function() {
		var bullet = new Asteroids.Bullet(this);
		this.game.add(bullet);
	}
})();