(function(){
  
  var Util = Asteroids.Util = Asteroids.Util || {};
  
  Util.inherits = function(subClass, superClass) {
    function Surrogate() {};
    Surrogate.prototype = superClass.prototype;
    subClass.prototype = new Surrogate();
  };
  
  Util.randomVec = function(length) {
    xVec = Math.floor(((Math.random() * 2) - 1) * (Math.random() * length) + 1);
    yVec = Math.floor(((Math.random() * 2) - 1) * (Math.random() * length) + 1);
    return [xVec, yVec];
  };
})();

// Asteroid

// Util.inherits.call(Asteroid, MovingObject)