(function(){
    
  var Asteroid = Asteroids.Asteroid = function (pos, game) {
    var COLOR = '#00ff00';
    var RADIUS = 10;
    var vec = Asteroids.Util.randomVec(10);
    var attributes =  {
      color: COLOR,
      radius: RADIUS,
      pos: pos,
      vel: vec,
      game: game,
      isWrappable: true
    };
    Asteroids.MovingObject.call(this, attributes);
  };
  
  Asteroids.Util.inherits(Asteroids.Asteroid, Asteroids.MovingObject);

  Asteroids.Asteroid.prototype.collideWith = function(obj) {
    if(obj instanceof Asteroids.Ship){
      obj.relocate();
    }
  }

})();


