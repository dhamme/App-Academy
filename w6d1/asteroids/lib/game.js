(function(){
  
  var Game = Asteroids.Game = function(){
    this.asteroidsArr = [];
    this.bulletsArr = [];
    this.addAsteroids();
    this.ship = new Asteroids.Ship(this.randomPosition(), this);

  }

  Asteroids.Game.DIM_X = 800;
  Asteroids.Game.DIM_Y = 600;
  Asteroids.Game.NUM_ASTEROIDS = 10;

  Asteroids.Game.prototype.randomPosition = function(){
    xPos = Math.floor((Math.random() * Asteroids.Game.DIM_X) + 1);
    yPos = Math.floor((Math.random() * Asteroids.Game.DIM_Y) + 1);
    return [xPos, yPos];
  }

  // when do we create a method on a prototype vs on the class?
  Asteroids.Game.prototype.addAsteroids = function() {
    for(var i = 1; i < Asteroids.Game.NUM_ASTEROIDS; i++){
      var ast = new Asteroids.Asteroid(this.randomPosition(), this);
      this.add(ast);
    }
  }

  Asteroids.Game.prototype.add = function(obj) {
    if(obj instanceof Asteroids.Asteroid){
      this.asteroidsArr.push(obj);
    }
    if(obj instanceof Asteroids.Bullet){
      this.bulletsArr.push(obj);
    }
  }

  Asteroids.Game.prototype.draw = function(ctx) {
    ctx.clearRect(0, 0, Asteroids.Game.DIM_X, Asteroids.Game.DIM_Y);
    this.allObjects().forEach(function(el){
      el.draw(ctx);
    })
  }

  Asteroids.Game.prototype.moveObjects = function() {
    this.allObjects().forEach(function(el){
      el.move();
    })
  }

  Asteroids.Game.prototype.wrap = function(pos) {
    var dimensions = [Asteroids.Game.DIM_X, Asteroids.Game.DIM_Y];
    var newPos = pos;
    newPos.forEach(function(coord, i){
      if(coord > dimensions[i]){
        newPos[i] = coord - dimensions[i];
      } else if(coord < 0){
        newPos[i] = coord + dimensions[i];
      } 
    })
    return newPos;
  }

  Asteroids.Game.prototype.checkCollisions = function() {
    var gameScope = this;
    gameScope.allObjects().forEach(function(outerEl) {
      gameScope.allObjects().forEach(function(innerEl) {
        if(outerEl !== innerEl && outerEl.isCollidedWith(innerEl)){
          outerEl.collideWith(innerEl);
        }
      })
    })
  }

  Asteroids.Game.prototype.remove = function(obj) {
    if(obj instanceof Asteroids.Asteroid){
      var index = this.asteroidsArr.indexOf(obj);
      this.asteroidsArr.splice(index, 1);
    }
    if(obj instanceof Asteroids.Bullet){
      var index = this.bulletsArr.indexOf(obj);
      this.bulletsArr.splice(index, 1);
    }
  }

  Asteroids.Game.prototype.allObjects = function() {
    return this.asteroidsArr.concat([this.ship], this.bulletsArr);
  }

  Asteroids.Game.prototype.isOutOfBounds = function(pos) {
    if(pos[0] > Asteroids.Game.DIM_X ||
      pos[0] < 0 ||
      pos[1] > Asteroids.Game.DIM_Y ||
      pos[1] < 0
    ) {
      return true;
    } else {
      return false;
    }
  }
})();


// var tempAsteroids = this.asteroidsArr;
//     // is there a more efficient way to check for collisions?
//     this.asteroidsArr.forEach(function(outerEl) {
//       if(tempAsteroids.indexOf(outerEl) !== -1){
//         // is in temp array
//         tempAsteroids.forEach(function(innerEl){
//           if(innerEl !== outerEl && innerEl.isCollidedWith(outerEl)){
//             alert("Collision");
            
//           }
//         })

//       } else {
//         // not in tempt array, already evaluated it, skip
//       }
//     })