(function(){
  
  var Asteroids = window.Asteroids = window.Asteroids || {};
  
  var MovingObject = Asteroids.MovingObject = function(attributes) {
    this.pos = attributes.pos;
    this.vel = attributes.vel;
    this.radius = attributes.radius;
    this.color = attributes.color;
    this.game = attributes.game;
    this.isWrappable = attributes.isWrappable;
  };
  
  MovingObject.prototype.draw = function(ctx) {
    ctx.fillStyle = this.color;
    ctx.beginPath();
    ctx.arc(
      this.pos[0], 
      this.pos[1], 
      this.radius, 
      0, 
      Math.PI * 2, 
      false
    );
    ctx.fill();
  };
  
  MovingObject.prototype.move = function() {
    this.pos[0] += this.vel[0];
    this.pos[1] += this.vel[1];
    if(this.game.isOutOfBounds(this.pos)){
      if(this.isWrappable){
        this.pos = this.game.wrap(this.pos);
      } else {
        this.game.remove(this);
      }
    }
  }

  MovingObject.prototype.isCollidedWith = function(obj) {
    var xLen = Math.abs(this.pos[0] - obj.pos[0]);
    var yLen = Math.abs(this.pos[1] - obj.pos[1]);
    var distance = Math.sqrt(Math.pow(xLen, 2) + Math.pow(yLen, 2));

    if(distance <= this.radius + obj.radius){
      return true;
    } else {
      return false;
    }
  }

  MovingObject.prototype.collideWith = function(obj) {
    // this.game.remove(obj);
    // this.game.remove(this);
  }
})();

