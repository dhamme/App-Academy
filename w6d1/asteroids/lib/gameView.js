(function(){
  
  var GameView = Asteroids.GameView = function(game, ctx){
    this.game = game;
    this.ctx = ctx;
  }

  GameView.prototype.start = function() {
    this.bindKeyHandlers(this.game);
  	setInterval(function(){
  		this.game.draw(this.ctx);
  		this.game.moveObjects();
  		this.game.checkCollisions();

  	}, 20);
  }

  GameView.prototype.bindKeyHandlers = function(game) {
    key('w', function(){
      game.ship.power([0, -3]);
    });
    key('a', function(){
      game.ship.power([-3, 0]);
    });
    key('s', function(){
      game.ship.power([0, 3]);
    });
    key('d', function(){
      game.ship.power([3, 0]);
    });
    key('space', function(){
      game.ship.fireBullet();
    });

  }
})();