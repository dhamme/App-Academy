(function(){
	var Bullet = Asteroids.Bullet = function(ship) {
		var radius = 2;
		var color = '#0000ff';
		var vel = ship.vel.slice(0);
		// the goal is to make bullets move at a constant which is faster
		// than the ships max speed, in any direction
		if(vel[0] === 0 && vel[1] === 0){
			vel = [0, -11];
		} else {
			if(vel[0] !== 0){
				vel[0] = (vel[0] / 10) * 11;
			}
			if(vel[1] !== 0){
				vel[1] = (vel[1] / 10) * 11;
			}
		}
		var attributes = {
			color: color,
      radius: radius,
      pos: ship.pos.slice(0),
      vel: vel,
      game: ship.game,
      isWrappable: false
		};
		Asteroids.MovingObject.call(this, attributes);
	}

	Asteroids.Util.inherits(Asteroids.Bullet, Asteroids.MovingObject);

	Bullet.prototype.collideWith = function(obj) {
		if(obj instanceof Asteroids.Asteroid){
      this.game.remove(obj);
      this.game.remove(this);
    }
	}
})();