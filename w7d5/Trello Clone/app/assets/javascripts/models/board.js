TrelloClone.Models.Board = Backbone.Model.extend({
	urlRoot: 'api/boards',

	lists: function(){
		this._lists || (this._lists = new TrelloClone.Collections.Lists());
		return this._lists; 
	},

	parse: function(resp){
		debugger
		if(resp.lists){
			this.lists().set(resp.lists, { parse: true });
		}
		// delete resp['lists'];

		return resp;
	}
});
