TrelloClone.Routers.BoardsRouter = Backbone.Router.extend({
	initialize: function(){
		this.$main = $('#main');
	},

	routes: {
		'': 'boardsIndex',
		'boards/:id': 'boardShow'
	},

	boardsIndex: function(){
		var view = new TrelloClone.Views.BoardsIndex({
			collection: TrelloClone.boards 
		});
		TrelloClone.boards.fetch();
		this.$main.html(view.render().$el) 
	},

	boardShow: function(id){
		var board = TrelloClone.boards.getOrFetch(id);
		var view = new TrelloClone.Views.BoardShow({
			model: board
		});
		board.fetch();
		this.$main.html(view.render().$el);
	}
});
