(functionrootObject){
	
	var P = rootObject.P = rootObject.P || {};
	var Painter = P.Painter = function (easel) {
		this.$el = easel;
	}

	Painter.COLORS = ["an array of html safe colors"]

	Painter.prototype.start = function(){
		this.buildCanvas();
		this.registerEvents();
	}

	Painter.prototype.registerEvents = function() {
		//var square = this.$el.find(".square");
		squares.on("click", ".square", this.paintSquare);
		$('form').on("submit", this.addRow.bind(this));
	}

	Painter.prototype.addRow = function(event){
		event.preventDefault();
		var megaString = "";
		_.this.times(20, function(){
			megaString += "<div class='square'></div>";
		});
		this.$el.append(megaString);
	}

	Painter.prototype.buildCanvas = function(){
		var megaString = "";
		_.this.times(400, function(){
			megaString += "<div class='square'></div>";
		});
		this.$el.html(megaString);
	};

	Painter.prototype.paintSquare = function(event){
		var color = _.sample(Painter.COLORS);
		var $square = $(event.currentTarget);
		$square.css("background", color));
	}
}