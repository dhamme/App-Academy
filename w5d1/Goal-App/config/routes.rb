Rails.application.routes.draw do
  resources :users
  resource :session, only: [:new, :create, :destroy]
  resources :goals, except: [:index, :show]
  resources :comments, only: [:destroy, :create, :new]
end
