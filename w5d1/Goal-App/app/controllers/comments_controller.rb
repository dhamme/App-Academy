class CommentsController < ApplicationController
  def new
    render :new
  end
  
  def create
    @comment = Comment.new(comment_params)
    @comment.author_id = current_user.id
    flash[:errors] = @comment.errors.full_messages unless @comment.save
    if @comment.commentable.is_a?(Goal)
      redirect_to user_url(@comment.commentable.user)
    else
      redirect_to user_url(@comment.commentable)
    end
  end
  
  def comment_params
    params.require(:comment).permit(:body, :commentable_id, :commentable_type)
  end

end
