class GoalsController < ApplicationController
  def new
    @goal = Goal.new
    render :new
  end
  
  def create
    @goal = current_user.goals.new(goal_params)
    
    flash[:errors] = @goal.errors.full_messages unless @goal.save         
    redirect_to user_url(current_user)
  end
  
  def edit
    @goal  = Goal.find(params[:id])
    render :edit
  end
  
  def update
    @goal = Goal.find(params[:id])
    
    if @goal.update(goal_params)
      redirect_to user_url(current_user)
    else
      flash.now[:errors] = @goal.errors.full_messages
      render :edit
    end
  end
  
  def destroy
    Goal.find(params[:id]).destroy
    redirect_to user_url(current_user)
  end
  
  def goal_params
    params.require(:goal).permit(:body, :privacy)
  end
end
