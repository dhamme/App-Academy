# == Schema Information
#
# Table name: goals
#
#  id         :integer          not null, primary key
#  body       :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#  privacy    :string(255)      not null
#

class Goal < ActiveRecord::Base
  include Commentable
  validates :body, :user_id, presence: true
  belongs_to :user
  has_many :comments, as: :commentable
end
