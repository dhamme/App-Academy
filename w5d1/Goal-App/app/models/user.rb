# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string(255)      not null
#  password_digest :string(255)      not null
#  session_token   :string(255)      not null
#  created_at      :datetime
#  updated_at      :datetime
#

class User < ActiveRecord::Base
  include Commentable
  attr_accessor :password
  validates :username, :session_token, presence: true
  validates :password, length: { minimum: 6, allow_nil: true }
  before_validation :ensure_session_token
  
  has_many :goals
  has_many :comments, as: :commentable
  
  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end
  
  def is_password?(password)
    BCrypt::Password.new(password_digest).is_password?(password)
  end
  
  def self.find_by_credentials(username, password)
    user = User.find_by(username: username)
    return nil unless user
    user.is_password?(password) ? user : nil
  end
  
  def reset_session_token!
    self.session_token = ensure_session_token
    save!
    self.session_token
  end
  
  def ensure_session_token
    self.session_token = generate_session_token
  end
  
  def generate_session_token
    SecureRandom.base64(16)
  end
  
end
