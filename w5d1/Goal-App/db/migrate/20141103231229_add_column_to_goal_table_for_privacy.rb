class AddColumnToGoalTableForPrivacy < ActiveRecord::Migration
  def change
    add_column :goals, :privacy, :string, null: false
  end
end
