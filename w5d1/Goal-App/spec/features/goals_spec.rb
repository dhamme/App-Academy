require 'rails_helper'

feature "goals" do
  before { sign_up_as_danny }
  let(:danny) { User.find_by(username: "danny") }
  
  feature "User can create a goal" do
    it "has a goal button" do
      expect(page).to have_button("Create Goal")
    end
  
    scenario "user makes new goal" do
      create_public_goal("I want to be a real boy")
      expect(page).to have_content("I want to be a real boy")
    end
      
      
  end

  feature "User pages displays goals" do
    scenario "User show page displays all goals owned by user" do
      create_public_goal("I'm a real boy")
      create_private_goal("Someday I will be real")
      expect(page).to have_content("I'm a real boy")
      expect(page).to have_content("Someday I will be real")
    end
  
    it "only shows public goals when logged out or not the goal's creator" do
      create_public_goal("People will know me as a human")
      create_private_goal("Escape the matrix")
      click_button("Sign Out")
      visit(user_url danny)
      expect(page).to have_content("People will know me as a human")
      expect(page).not_to have_content("Escape the matrix")
    end
  end

  feature "User can delete goal" do
    scenario "user can delete own goals" do
      create_private_goal("Rule the world")
      click_button("Delete Goal")
      expect(page).not_to have_content("Rule the world")
    end
    
    scenario "user cannot delete other users' goals" do
      create_public_goal("Be less cool")
      click_button("Sign Out")
      visit(new_user_url) 
      sign_up_as("Felix")
      visit(user_url(danny))
      expect(page).to have_content("Be less cool")
      expect(page).not_to have_button("Delete Goal")
    end
  
    scenario "user is redirected to own show page upon successful deletion" do
      create_private_goal("Rule the world")
      click_button("Delete Goal")
      expect(current_url).to eq(user_url danny)
    end

  end

  feature "User can update goal" do
    scenario "user can update own goals" do
      create_public_goal("Be friendly to animals")
      edit_goal("Rule the world", "private")
      expect(page).to have_content("Rule the world")
    end
    
    scenario "user cannot update other user's goal" do
      create_public_goal("Give flowers to pretty girls")
      click_button("Sign Out")
      sign_up_as("I hate girls")
      visit(user_url(danny))
      expect(page).not_to have_button("Update Goal")
    end
    
    scenario "user is redirected to updated goal's page after update" do
      create_public_goal("Buy flowers")
      edit_goal("Steal flowers from graveyard", "private")
      expect(current_url).to eq(user_url(danny))
    end
  end
end