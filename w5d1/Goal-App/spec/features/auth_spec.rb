require 'rails_helper'

feature "the signup process" do 

  scenario "has a new user page" do
    visit('/users/new')
    expect(page).to have_content "New user"
  end
  
  feature "signing up a user" do

    scenario "shows username on the homepage after signup" do
      sign_up_as_danny
      expect(page).to have_content "danny"
    end

  end

end

feature "logging in" do 
  before { sign_up_as_danny }
  
  scenario "shows username on the homepage after login" do
    visit('/session/new')
    fill_in('username', with: "danny")
    fill_in('password', with: "dannydanny")
    click_button("Sign In")
    expect(page).to have_content "danny"
  end

end

feature "logging out" do     

  scenario "doesn't show username on the homepage when not logged in" do
    sign_up_as_danny
    click_button("Sign Out")
    expect(page).not_to have_content "danny"
  end

end