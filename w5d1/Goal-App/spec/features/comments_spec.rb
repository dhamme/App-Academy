require 'rails_helper'

feature "comments" do
  before { sign_up_as_danny }
  let(:danny) { User.find_by(username: "danny") }
  
  scenario "User can post comments on their own page" do
    create_page_comment("I am me")
    expect(page).to have_content("I am me")
  end
    
  
  scenario "User can post comments on their own goals" do
    create_public_goal("Finish RSPEC")
    create_goal_comment("That's a really bad goal, man. Seriously.")
    expect(page).to have_content("That's a really bad goal, man. Seriously.")
  end
  
  scenario "User can post comments on other users' pages" do
    click_button("Sign Out")
    sign_up_as('Felix Leiter')
    visit(user_url(danny))
    create_page_comment('Watch out for sharks')
    expect(page).to have_content('Watch out for sharks')
  end


  scenario "User can post comments on other users' goals" do
    create_public_goal("Does anyone know where I can get a golden gun?")
    click_button("Sign Out")
    sign_up_as('Felix Leiter')
    visit(user_url(danny))
    create_goal_comment('I might know a guy')
    expect(page).to have_content('I might know a guy')
  end

  scenario "Deleting a user's goal deletes the goal's comments" do
    create_private_goal("Sell stash of golden bullets")
    create_goal_comment("Felix knows a guy who will buy in packs of six or so")
    click_button("Delete Goal")
    expect(page).not_to have_content("Felix knows a guy who will buy in packs of six or so")
  end
end