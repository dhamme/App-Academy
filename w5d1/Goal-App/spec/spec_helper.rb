require 'rails_helper'

RSpec.configure do |config|

  config.expect_with :rspec do |expectations|

    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end


  config.mock_with :rspec do |mocks|

    mocks.verify_partial_doubles = true
  end

end

def sign_up_as(username)
  visit('/users/new')
  fill_in('username', with: username)
  fill_in('password', with: username*2 )
  click_button("Create User")
end

def sign_up_as_danny
  sign_up_as('danny')
end

def create_page_comment(text)
  fill_in('body', with: text)
  click_button('submit')
end

def create_goal_comment(text)
  find(:css, 'li textarea').set(text)
  find(:css, 'li form input.submit').click
end

def create_public_goal(text)
  click_button("Create Goal")
  fill_in('body', with: text)
  choose('public')
  click_button('Submit Goal')
end

def create_private_goal(text)
  click_button("Create Goal")
  fill_in('body', with: text)
  choose('private')
  click_button('Submit Goal')
end

def edit_goal(text, choice)
  click_button("Edit Goal")
  fill_in('body', with: text)
  choose(choice)
  click_button("Update Goal")
end