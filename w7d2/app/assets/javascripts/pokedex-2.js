Pokedex.RootView.prototype.addToyToList = function (toy) {
  var shortInfo = ['name', 'happiness', 'price'];
  var addToyTemplate = $("#toy-list-item-template").html();
  var templateFn = _.template(addToyTemplate);
  var renderedContent = templateFn({ toy: toy, shortInfo: shortInfo });
  this.$pokeDetail.find(".toys").append(renderedContent);
};

Pokedex.RootView.prototype.renderToyDetail = function (toy) { // III
  this.$toyDetail.empty();
  var pokes = this.pokes;
  var toyTemplate = $('#toy-detail-template').html();
  var toyFn = _.template(toyTemplate);
  var renderedContent = toyFn({ toy: toy, pokes: pokes})
  //
  // var $detail = $('<div class="detail">');
  // $detail.append('<img src="' + toy.get('image_url') + '"><br>');
  // for (var attr in toy.attributes) {
  //   if(attr !== 'pokemon_id' && attr !== 'image_url') {
  //     var $span = $('<span style="font-weight:bold;">');
  //     $span.html(attr + ': ');
  //     $detail.append($span);
  //     $detail.append(toy.get(attr));
  //     $detail.append('<br>');
  //   }
  // }
  //
  // // Phase III
  // var $pokemonSelect = $('<select>');
  // $pokemonSelect.data("pokemon-id", toy.get("pokemon_id"));
  // $pokemonSelect.data("toy-id", toy.id);
  // this.pokes.each(function (pokemon) {
  //   var $pokemonOption = $('<option>');
  //   $pokemonOption.attr("value", pokemon.id);
  //   $pokemonOption.text(pokemon.get("name"));
  //   if (pokemon.id == toy.get("pokemon_id")) {
  //     $pokemonOption.prop("selected", true);
  //   }
  //   $pokemonSelect.append($pokemonOption);
  // });
  // $detail.append($pokemonSelect);

  this.$toyDetail.html(renderedContent);
};

Pokedex.RootView.prototype.selectToyFromList = function (event) {
  var $target = $(event.target);

  var toyId = $target.data('id');
  var pokemonId = $target.data('pokemon-id');

  var pokemon = this.pokes.get(pokemonId);
  var toy = pokemon.toys().get(toyId);

  this.renderToyDetail(toy);
};
