Pokedex.RootView.prototype.addPokemonToList = function (pokemon) {
  var listItemTemplate = $('#pokemon-list-item-template').html();
  var templateFn = _.template(listItemTemplate);
  var renderedContent = templateFn({ pokemon: pokemon })
   
  this.$pokeList.append(renderedContent);
};

Pokedex.RootView.prototype.refreshPokemon = function (callback) {
  this.pokes.fetch({
    success: (function () {
      this.$pokeList.empty();
      this.pokes.each(this.addPokemonToList.bind(this));
      callback && callback();
    }).bind(this)
  });

  return this.pokes;
};
