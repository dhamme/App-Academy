class QuestionFollower
  
  attr_reader :id, :user_id, :question_id
  def initialize(hash)
    @id = hash['id']
    @user_id = hash['user_id']
    @question_id = hash['question_id']
  end
  
  def self.find_by_id(id)
    query = <<-SQL
    SELECT 
      *
    FROM 
      question_followers
    WHERE
    id = ?
    SQL
    hash = QuestionsDatabase.instance.execute(query, id).first
    QuestionFollower.new(hash)
  end
  
  def self.followers_for_question_id(question_id)
    query = <<-SQL
    SELECT
      *
    FROM
      users
    JOIN
      question_followers
    ON
      users.id = question_followers.user_id
    WHERE
      question_id = ?
    SQL
    hashes = QuestionsDatabase.instance.execute(query, question_id)
    array_of_followers = []
    hashes.each do |hash|
      array_of_followers << User.new(hash)
    end
    array_of_followers
  end
  
  def self.followed_questions_for_user_id(user_id)
    query = <<-SQL
    SELECT
      *
    FROM
      questions
    LEFT OUTER JOIN
      question_followers
    ON
      questions.id = question_followers.question_id
    WHERE
      question_followers.user_id = ?
    SQL
    hashes = QuestionsDatabase.instance.execute(query, user_id)
    array_of_questions = []
    hashes.each do |hash|
      array_of_questions << Question.new(hash)
    end
    array_of_questions
  end
  
  def self.most_followed_questions(n)
    query = <<-SQL
    SELECT
      *
    FROM 
      questions
    JOIN 
      question_followers
    ON 
      questions.id = question_followers.question_id
    GROUP BY 
      questions.id
    ORDER BY 
      COUNT(*) DESC
    SQL
    hashes = QuestionsDatabase.instance.execute(query)
    array_of_questions = []
    n.times do |x|
      array_of_questions << Question.new(hashes[x]) unless hashes[x].nil?
    end
    array_of_questions
  end
end
