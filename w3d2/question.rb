

class Question
  
  attr_reader :id, :user_id
  attr_accessor :title, :body
  def initialize(hash)
    @id = hash['id']
    @title = hash['title']
    @body = hash['body']
    @user_id = hash['user_id']
  end
  
  def self.find_by_id(id)
    query = <<-SQL
    SELECT
      *
    FROM
      questions
    WHERE
      id = ?;
    SQL
    hash = QuestionsDatabase.instance.execute(query, id).first
    Question.new(hash)
  end
  
  def self.most_followed(n)
    QuestionFollower.most_followed_questions(n)
  end
  
  def self.most_liked(n)
    QuestionLike.most_liked_questions(n)
  end
  
  def self.find_by_author_id(author_id)
    query = <<-SQL
    SELECT
      *
    FROM
      questions
    WHERE
      user_id = ?;
    SQL
    hashes = QuestionsDatabase.instance.execute(query, author_id)
    array_of_questions = []
    hashes.each do |hash|
      array_of_questions << Question.new(hash)
    end
    array_of_questions
  end
  
  def author
    User.find_by_id(user_id)
  end
  
  def replies
    Reply.find_by_question_id(@id)
  end
  
  def followers
    QuestionFollower.followers_for_question_id(@id)
  end
  
  def likers
    QuestionLike.likers_for_question_id(@id)
  end
  
  def num_likes
    QuestionLike.num_likes_for_question_id(@id)
  end
end