CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname VARCHAR(255) NOT NULL,
  lname VARCHAR(255) NOT NULL
);

CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  body TEXT NOT NULL,
  user_id INTEGER NOT NULL,
  FOREIGN KEY (user_id ) REFERENCES users(id)
);

CREATE TABLE question_followers (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  body TEXT NOT NULL,
  parent_id INTEGER,
  author_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY (parent_id) REFERENCES replies(id),
  FOREIGN KEY (author_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

CREATE TABLE question_likes (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

-- Seed Data
INSERT INTO
  users (fname, lname)
VALUES
('Jim', 'Bob'),  ('Dude', 'Mang'), ('Ray', 'Charles');

INSERT INTO 
  questions (title, body, user_id)
VALUES
  ('Question 1', 'Questions...how do they work?', 1),
  ('quest2', 'where am I?', 3),
  ("The third and most important question", "What is your favorite color?", 1);
  
INSERT INTO
  question_followers (user_id, question_id)
VALUES
  (2, 1), (2, 3);

INSERT INTO
  question_likes (user_id, question_id)
VALUES
  (2,2);
  
INSERT INTO 
  replies (body, parent_id, author_id, question_id)
VALUES
  ('dude, ur lyke totally blind, mang', NULL, 2, 2);

INSERT INTO
  replies (body, parent_id, author_id, question_id)
VALUES
  ('dude, you are like a mang, dude', 1, 2, 2);
  
  