class Reply
  attr_accessor :body
  attr_reader :id, :parent_id, :question_id, :author_id
  def initialize(hash)
    @id = hash['id']
    @parent_id = hash['parent_id']
    @question_id = hash['question_id']
    @author_id = hash['author_id']
    @body = hash['body']
  end
  
  def self.find_by_id(id)
    query = <<-SQL
    SELECT 
      *
    FROM 
      replies
    WHERE
      id = ?
    SQL
    hash = QuestionsDatabase.instance.execute(query, id).first
    Reply.new(hash)
  end
  
  def self.find_by_question_id(question_id)
    query = <<-SQL
    SELECT
      *
    FROM
      replies
    WHERE
      question_id = ?
    SQL
    array_of_replies = []
    hashes = QuestionsDatabase.instance.execute(query, question_id)
    hashes.each do |hash|
      array_of_replies << Reply.new(hash)
    end
    array_of_replies
  end
  
  def self.find_by_user_id(user_id)
    query = <<-SQL
    SELECT
      *
    FROM
      replies
    WHERE
      author_id = ?
    SQL
    array_of_replies = []
    hashes = QuestionsDatabase.instance.execute(query, user_id)
    hashes.each do |hash|
      array_of_replies << Reply.new(hash)
    end
    array_of_replies
  end
  
  def author
    User.find_by_id(author_id)
  end
  
  def question
    Question.find_by_id(question_id)
  end
  
  def parent_reply
    Reply.find_by_id(parent_id)
  end
  
  def child_replies
    query = <<-SQL
    SELECT
      *
    FROM
      replies
    WHERE
      parent_id = ?
    SQL
    reply_children = []
    hashes = QuestionsDatabase.instance.execute(query, @id)
    hashes.each do |hash|
      reply_children << Reply.new(hash)
    end
    reply_children
  end
end