class QuestionLike
  attr_reader :id, :user_id, :question_id
  def initialize(hash)
    @id = hash['id']
    @user_id = hash['user_id']
    @question_id = hash['question_id']
  end
  
  def self.find_by_id(id)
    query = <<-SQL
    SELECT 
      *
    FROM 
      question_likes
    WHERE
    id = ?
    SQL
    hash = QuestionsDatabase.instance.execute(query, id).first
    QuestionLike.new(hash)
  end
  
  def self.likers_for_question_id(question_id)
    query = <<-SQL
    SELECT
      *
    FROM
      question_likes
    JOIN
      users
    ON
      users.id = question_likes.user_id
    WHERE
      question_likes.question_id = ?
    SQL
    hashes = QuestionsDatabase.instance.execute(query, question_id)
    array_of_likers = []
    hashes.each do |hash|
      array_of_likers << User.new(hash)
    end
    
    array_of_likers
  end
  
  def self.num_likes_for_question_id(question_id)
    query = <<-SQL
    SELECT
      COUNT(*) count
    FROM
      question_likes
    WHERE
      question_likes.question_id = ?
    SQL
    QuestionsDatabase.instance.execute(query, question_id).first['count']
  end
  
  def self.liked_questions_for_user_id(user_id)
    query = <<-SQL
    SELECT
      *
    FROM
      question_likes
      JOIN
      questions
      ON 
      questions.id = question_likes.question_id
    WHERE
      question_likes.user_id = ?
    SQL
    hashes = QuestionsDatabase.instance.execute(query, user_id)
    array_of_questions = []
    hashes.each do |hash|
      array_of_questions << Question.new(hash)
    end
    array_of_questions
  end
  
  def self.most_liked_questions(n)
    query = <<-SQL
    SELECT
      *
    FROM 
      questions
    JOIN 
      question_likes
    ON 
      questions.id = question_likes.question_id
    GROUP BY 
      questions.id
    ORDER BY 
      COUNT(*) DESC
    LIMIT 
      ?
    SQL
    hashes = QuestionsDatabase.instance.execute(query, n)
    array_of_questions = []
    hashes.each do |hash|
      array_of_questions << Question.new(hash)
    end
    array_of_questions
  end
  
end