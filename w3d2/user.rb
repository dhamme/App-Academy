require 'singleton'
require 'sqlite3'
require 'active_support/core_ext/string'

require_relative 'question'
require_relative 'question_follower'
require_relative 'question_like'
require_relative 'reply'


class QuestionsDatabase < SQLite3::Database
  include Singleton
  
  def initialize
    super('aa.db')
    self.results_as_hash = true
    self.type_translation = true
  end
end

class User
  
  attr_reader :id
  attr_accessor :fname, :lname
  def initialize(hash)
    @id = hash['id']
    @fname = hash['fname']
    @lname = hash['lname']
  end
  
  def self.find_by_id(id)
    query = <<-SQL
    SELECT
      *
    FROM
      users
    WHERE
      id = ?;
    SQL
    hash = QuestionsDatabase.instance.execute(query, id).first
    User.new(hash)
  end
  
  def self.find_by_name(fname, lname)
    options = { :fname => fname, :lname => lname }
    query = <<-SQL
    SELECT
      *
    FROM
      users
    WHERE
      fname = :fname
    AND
      lname = :lname;
    SQL
    hash = QuestionsDatabase.instance.execute(query, options).first
    User.new(hash)
  end
  
  def authored_questions
    query = <<-SQL
    SELECT
      *
    FROM
      questions
    WHERE
      user_id = ?
    SQL
    array_of_questions = []
    hashes = QuestionsDatabase.instance.execute(query, @id)
    hashes.each do |hash|
      array_of_questions << Question.new(hash)
    end
    array_of_questions
  end
  
  def authored_replies
    query = <<-SQL
    SELECT
      *
    FROM
      replies
    WHERE
      author_id = ?
    SQL
    array_of_replies = []
    hashes = QuestionsDatabase.instance.execute(query, @id)
    hashes.each do |hash|
      array_of_replies << Reply.new(hash)
    end
    array_of_replies
  end
  
  def followed_questions
    QuestionFollower.followed_questions_for_user_id(@id)
  end
  
  def liked_questions
    QuestionLike.liked_questions_for_user_id(@id)
  end
  
  def average_karma
    query = <<-SQL
    SELECT
      (COUNT(DISTINCT questions.id) / CAST(COUNT(question_likes.id) AS FLOAT)) AS AVG
    FROM
      questions
    LEFT OUTER JOIN
     question_likes
    ON
      questions.id = question_likes.question_id
    WHERE
    questions.user_id = ?
    SQL
    QuestionsDatabase.instance.execute(query, @id).first['AVG']
  end
  
  def save
    options = {}
    self.instance_variables.each do |variable|
      options[variable.to_s.delete("@").intern] = self.instance_variable_get(variable)
    end
    
    if @id.nil?
      options.delete(:id)
      
      insert_row = "#{self.class.to_s.downcase.pluralize} (#{options.keys.sort.join(", ")})"
      values_row = "(:#{options.keys.sort.join(", :")})"
      
      query = <<-SQL
      INSERT INTO
        #{insert_row}
      VALUES
        #{values_row}
      SQL
      QuestionsDatabase.instance.execute(query, options)
      @id = QuestionsDatabase.instance.last_insert_row_id
    else
      # FIX THIS
      insert_row = "#{self.class.to_s.downcase.pluralize}"
      values_row = "(:#{options.keys.sort.join(", :")})"
      
      query = <<-SQL
      UPDATE
        #{insert_row}
      VALUES
        #{values_row}
      SQL
      QuestionsDatabase.instance.execute(query, options)
    end
  end
  
end



# options = { :fname => @fname, :lname => @lname, :id => @id }
# query = <<-SQL
# UPDATE
#   users
# SET
#   fname = :fname, lname = :lname
# WHERE
#   id = :id
# SQL
# QuestionsDatabase.instance.execute(query, options)

# u = User.find_by_id(3)
# p u.average_karma
# p u.liked_questions
# q = Question.find_by_id(1)
# p q
# qf = QuestionFollower.find_by_id(1)
# p qf
# r = Reply.find_by_id(1)
# p r
# ql = QuestionLike.find_by_id(1)
# p ql

# u = User.find_by_name('Ray', 'Charles')
# p u.authored_questions
# u2 = User.find_by_name('Dude', "Mang")
# u2.authored_replies
# p Question.find_by_author_id(3).first.replies
# p Reply.find_by_question_id(2).first.id

# p QuestionFollower.followers_for_question_id(1)
# p QuestionFollower.followed_questions_for_user_id(1)
# p QuestionLike.most_liked_questions(1)
# q = Question.find_by_id(2)
# p q.num_likes
# p Question.most_liked(1)
u = User.new({'fname' => 'Dr.', 'lname' =>'Watson'})
u.save

