# == Schema Information
#
# Table name: cats
#
#  id          :integer          not null, primary key
#  birth_date  :datetime         not null
#  color       :string(255)
#  name        :string(255)      not null
#  sex         :string(1)        not null
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Cat < ActiveRecord::Base
  COLORS = %w{black white brown tabby}
  
  validates :color, inclusion: { in: COLORS,
     message: "%{value} is not a valid color" }
  validates :sex, inclusion: { in: %w(M F),
    message:  "Sex must be M or F" }
  validates :birth_date, :name, :sex, :color, presence: true 
  validates :birth_date, :timeliness => 
  {:on_or_before => :now, :type => :date}
  
  has_many(
    :cat_rental_requests,
    class_name: "CatRentalRequest",
    foreign_key: :cat_id,
    primary_key: :id,
    dependent: :destroy
  )
  
  def age
    Time.now - :birth_date
  end
  

  
end
