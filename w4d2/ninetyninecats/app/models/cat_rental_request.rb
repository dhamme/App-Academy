# t.integer :cat_id, null: false
# t.datetime :start_date, null: false
# t.datetime :end_date, null: false
# t.string :status, default: "PENDING"


class CatRentalRequest < ActiveRecord::Base
  
  
  validates :status, inclusion: {in: %w(PENDING APPROVED DENIED), 
    message: "Status must be PENDING, APPROVED, or DENIED"}
  validates :cat_id, :start_date, :end_date, presence: true
  validate :status, :overlapping_approved_requests
  validates :end_date, :timeliness => 
  {:after => :start_date, :type => :date}

  belongs_to(
    :cat,
    class_name: 'Cat',
    foreign_key: :cat_id,
    primary_key: :id
  )
  
  def approve!
    CatRentalRequest.transaction do
      self.status = 'APPROVED'
      self.save!
      overlapping_pending_requests.each do |request| 
        request.deny!
      end
    end
  end
  
  def deny!
    request.status = 'DENIED'
    request.save!
  end
    
  private 

  def overlapping_requests
    CatRentalRequest
    .where('start_date BETWEEN ? AND ?', self.start_date, self.end_date)
    .where('end_date BETWEEN ? AND ?', self.start_date, self.end_date)
    .where('cat_id = ?',  self.cat_id)
  end

  def overlapping_approved_requests
    if overlapping_requests.any?{ |request| request.status = 'APPROVED' }
      errors[:status] << "There are overlapping requests"
    end
  end
  
  def overlapping_pending_requests
    overlapping_requests.where('status = ?', 'PENDING')
  end  
  

end


