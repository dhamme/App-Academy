class CatRentalRequestsController < ApplicationController
  
  def new
    @cat_rental_request = CatRentalRequest.new
    render :new
  end
  
  def show
    @cat_rental_request = CatRentalRequest.find(params[:id])
    render :show
  end
  
  def create
    @cat_rental_request = CatRentalRequest.new(rental_params)
    if @cat_rental_request.save
      redirect_to cat_rental_request_url(@cat_rental_request)
    else
      render :new
    end
  end
  
  def edit
    @cat_rental_request = CatRentalRequest.find(params[:id])
    render :edit
  end
  
  def update
    @cat_rental_request = CatRentalRequest.find(params[:id])
    if @cat_rental_request.update(rental_params)
      redirect_to cat_rental_request_url(@cat_rental_request)
    else
      render :edit
    end
  end
  
  protected
  
  def rental_params
    params[:cat_rental_request].permit(:cat_id,:start_date, :end_date, :status)
  end
end
