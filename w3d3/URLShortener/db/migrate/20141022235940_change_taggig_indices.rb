class ChangeTaggigIndices < ActiveRecord::Migration
  def change
    remove_index :taggings, column: :tag_topic_id, unique: true
    remove_index :taggings, column: :shortened_url_id, unique: true
    
    add_index :taggings, :tag_topic_id
    add_index :taggings, :shortened_url_id
  end
end
