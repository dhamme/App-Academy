class ShortenedUrl < ActiveRecord::Base
  validates :short_url, :presence => true, :uniqueness => true
  
  validates :long_url, length: { maximum: 255 }
  
  validate :no_more_than_5_submissions_per_minute

  def self.create_long_url!(user, long_url)
    ShortenedUrl.create!(
      :submitter_id => user.id, 
      :long_url => long_url, 
      :short_url => self.random_code)
  end

  def self.random_code
    random_code = SecureRandom.urlsafe_base64
    
    while ShortenedUrl.exists?(:short_url => random_code)
      random_code = SecureRandom.urlsafe_base64
    end
    random_code
  end
  
  def num_clicks
    visits.count
  end
  
  def num_uniques
    visitors.count
  end
  
  def num_recent_uniques
    visitors.where('visits.created_at > ?', 30.seconds.ago).count
  end
  
  belongs_to(
    :submitter,
    class_name: 'User',
    :foreign_key => :submitter_id,
    :primary_key => :id
  )
  
  has_many(
    :visits,
    class_name: 'Visit',
    :foreign_key => :visited_url_id,
    :primary_key => :id
  )
  
  has_many(
    :visitors,
    Proc.new { distinct },
    :through => :visits,
    :source => :visitor
  )
  
  has_many(
    :taggings,
    class_name: 'Tagging',
    :foreign_key => :shortened_url_id,
    :primary_key => :id
  )
  
  has_many(
    :tag_topics,
    :through => :taggings,
    :source => :tag_topic
  )
  
  private
  def no_more_than_5_submissions_per_minute
    if submitter.submitted_urls.where('created_at > ?', 1.minute.ago).count > 5
      errors[:base] << "No more than 5 new urls per minute, dood."
    end
  end

end