class TagTopic < ActiveRecord::Base
  
  def most_popular
    # Multiple queries: 
    # short_url_objs =
#     shortened_urls.sort_by(&:num_clicks).reverse.take(3)

    # One SQL query:
    short_url_objs = shortened_urls
    .joins('LEFT OUTER JOIN visits ON shortened_urls.id = visits.visited_url_id')
    .group('shortened_urls.id')
    .order('COUNT(visits.id) DESC')
    
    short_url_objs.map { |x| x.long_url }
  end
  
  has_many(
  :taggings,
  class_name: 'Tagging',
  :foreign_key => :tag_topic_id,
  :primary_key => :id
  )
  
  has_many(
  :shortened_urls,
  :through => :taggings,
  :source => :shortened_url
  )
  
end