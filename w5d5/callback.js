function Clock () {
}

Clock.TICK = 5000;

Clock.prototype.printTime = function () {
  // Format the time in HH:MM:SS
  var hour = this.clockCurrentTime.getHours();
  var minutes = this.clockCurrentTime.getMinutes();
  var seconds = this.clockCurrentTime.getSeconds();
  
  console.log(hour + ":" + minutes + ":" + seconds);
};

Clock.prototype.run = function () {
  // 1. Set the currentTime.
  this.clockCurrentTime = new Date();
  this.printTime();
  setInterval(this._tick.bind(this), Clock.TICK);
  // 2. Call printTime.
  // 3. Schedule the tick interval.
};

Clock.prototype._tick = function () {
  this.clockCurrentTime.setTime(this.clockCurrentTime.getTime() + Clock.TICK);
  this.printTime();
  // 1. Increment the currentTime.
  // 2. Call printTime.
};

// clock = new Clock();
// clock.run();
