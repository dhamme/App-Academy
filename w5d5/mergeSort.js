var mergeSort = function(arr) {
	if(arr.length < 2){
		return arr;
	} else {
		var mid = Math.floor(arr.length / 2);
		var leftHalf = arr.slice(0, mid);
		var rightHalf = arr.slice(mid);
		return mergeHelper(mergeSort(leftHalf), mergeSort(rightHalf));
	}
};

var mergeHelper = function(left, right) {
	var sortedArr = [];
	while(left.length > 0 && right.length > 0){
		if(left[0] > right[0]){
			sortedArr.push(right.shift());
		} else {
			sortedArr.push(left.shift());
		}
	}
	return sortedArr.concat(left, right);
}