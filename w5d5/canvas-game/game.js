var canvas = document.getElementById('canvas');
// shorthand for 'context'
var ctx = canvas.getContext('2d');
var time = Date.now();
var itemCounter = 0;
var mySprite = {
	x: 200,
	y: 200,
	width: 64,
	height: 64,
	// pixels per second
	speed: 200, 
	// refers to frames in our png
	state: 0
}
var item = {
    x: Math.random() * canvas.width,
    y: Math.random() * canvas.height,
    width: 10,
    height: 10,
    color: '#fff'
}
var pacmanTiles = {
    loaded: false,
    image: new Image(),
    tileWidth: 64,
    tileHeight: 64
}
pacmanTiles.image.onload = function() {
   pacmanTiles.loaded = true;
}
pacmanTiles.image.src = 'pacman.png';

canvas.width = 800;
canvas.height = 600;

function update(mod) {
  if (37 in keysDown) { //left
  	mySprite.state = 2;
    mySprite.x -= mySprite.speed * mod; 
  }
  if (38 in keysDown) { //up
  	mySprite.state = 3;
    mySprite.y -= mySprite.speed * mod;
  }
  if (39 in keysDown) { //right
  	mySprite.state = 0;
    mySprite.x += mySprite.speed * mod;
  }
  if (40 in keysDown) { //down
  	mySprite.state = 1;
    mySprite.y += mySprite.speed * mod;
  }

  if (
    mySprite.x < item.x + item.width &&
    mySprite.x + mySprite.width > item.x &&
    mySprite.y < item.y + item.height &&
    mySprite.y + mySprite.height > item.y
	) {
    item.x = Math.random() * canvas.width;
    item.y = Math.random() * canvas.height;
    itemCounter ++;
	}
}

function render() {
	ctx.fillStyle = '#000';
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	ctx.fillStyle = item.color;
  ctx.fillRect(item.x, item.y, item.width, item.height);

  ctx.font = '12pt Arial';
	ctx.fillStyle = '#fff';
	ctx.textBaseline = 'top';
	ctx.fillText(itemCounter, 10, 10);

  if (pacmanTiles.loaded) {
    ctx.drawImage(
      pacmanTiles.image,  // image
      mySprite.state * pacmanTiles.tileWidth,  // source x, a function of state
      0,  // source y
      mySprite.width,  // tile width
      mySprite.height,  // tile height
      Math.round(mySprite.x),  // destination x
      Math.round(mySprite.y),  // destination y
      mySprite.width,  // destination width
      mySprite.height  // destination height
    );
}
}

function run(){
	update((Date.now() - time) / 1000);
	render();
	time = Date.now()
}

setInterval(run, 10);

var keysDown = {};
window.addEventListener('keydown', function(e){
	keysDown[e.keyCode] = true;
});
window.addEventListener('keyup', function(e){
	delete keysDown[e.keyCode];
});

