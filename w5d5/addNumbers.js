var readline = require('readline');

var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// reader.question("What is your name?", function (answer) {
//   console.log("Hello " + answer + "!");
// });

function addNumbers(sum, numsLeft, completionCallBack) {
  if(numsLeft > 0) {
    reader.question("Give a number ", function (answer){
      answer = parseInt(answer);
      sum += answer;
      console.log(answer);
      numsLeft -= 1;
      addNumbers(sum, numsLeft, completionCallBack);
    });  
  } else {
    completionCallBack(sum);
    reader.close();
  }
};

addNumbers(0, 2, function(sum){
  console.log("The sum is " + sum);
  
});