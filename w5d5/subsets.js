var subsets = function(arr) {
	if(arr.length === 0){
		return [[]];
	} else {
		var subs = subsets(arr.slice(0, arr.length - 1));
		return subs.concat(subs.map(function(el){
			return el.concat(arr[arr.length - 1]);
		}));
	}
};
