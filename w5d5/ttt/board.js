

function Board() {
  this.squares = [];
  for(var i = 0; i < 3; i++){
    this.squares.push(new Array(3));
    for(var j = 0; j < 3; j++){
      this.squares[i][j] = null;
    }
  }
};

Board.prototype.won = function(){
  var downDiag = [[0, 0], [1, 1], [2, 2]];
  var upDiag = [[0, 2], [1, 1], [2, 0]];
  var allCombinations = [];
  for(var i = 0; i < 3; i++){
    allCombinations.push([[i, 0], [i, 1], [i, 2]]);
    allCombinations.push([[0, i], [1, i], [2, i]]);
  };
  allCombinations.push(downDiag);
  allCombinations.push(upDiag);
  var that = this;
  var won = false;
  allCombinations.forEach(function(diag){
    if(that.squares[diag[0][0]][diag[0][1]] ===
    that.squares[diag[1][0]][diag[1][1]] &&
    that.squares[diag[1][0]][diag[1][1]] ===
    that.squares[diag[2][0]][diag[2][1]] &&
    that.squares[diag[0][0]][diag[0][1]] !== null){
      won = true;
    }
  });
  return won;
};

Board.prototype.winner = function(){
  //current_mover if won
};

Board.prototype.empty = function(pos){
  return this.squares[pos[0]][pos[1]] === null;
};

Board.prototype.placeMark = function(pos, mark){
  if(this.empty(pos)){
    this.squares[pos[0]][pos[1]] = mark;
    return true;
  } else {
    return false;
  }
};

Board.prototype.print = function(){
  this.squares.forEach(function(row){
    console.log(row);
  })
};

module.exports = Board;

// var board = new Board;
// console.log(board.checkDiag());
// board.print();