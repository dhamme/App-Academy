var Board = require('./board');

function Game(reader, player1, player2){
  this.player1 = player1;
  this.player2 = player2;
  this.currentPlayer = player1;
  this.board = new Board;
  this.reader = reader;
};

Game.prototype.run = function(completionCallback){
  if(this.board.won()){
    this.board.print();
    completionCallback();
    this.reader.close();
  } else{
    this.board.print();
    var that = this;
    reader.question("Which row would you like to move to?", function (answer) {
      var row = parseInt(answer) - 1;
      reader.question("Which column would you like to move to?",    
            function(answer){
      var col = parseInt(answer) - 1;
      if(that.board.placeMark([row,col], that.currentPlayer.mark)){
        that.currentPlayer = (that.currentPlayer === that.player1 ? that.player2 : that.player1);
      } else {
        console.log("invalid move");
      }
     that.run(completionCallback); 
    }) 
  })
}};
module.exports = Game;
