var readline = require('readline');
var Game = require("./game");

// var Game = app.Game;
// var Board = app.Board;

var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


function Player(name, mark){
  this.name = name;
  this.mark = mark;
}

var player1 = new Player("markov", "X");
var player2 = new Player("Curie", "O");

var game = new Game(reader, player1, player2);
game.run(function(){
  console.log("You Win");
});