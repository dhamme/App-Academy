var readline = require('readline');

var reader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function HanoiGame() {
  this.stacks = [[1,2,3],[],[]];
};

HanoiGame.prototype.isWon = function(){
  if(this.stacks[0].length === 0 && this.stacks[1].length === 0) {
    return true;
  } else {
    return false;
  }
};

HanoiGame.prototype.isValidMove = function(startTowerIdx, endTowerIdx) {
  if(this.stacks[endTowerIdx][0] > this.stacks[startTowerIdx][0] || this.stacks[endTowerIdx].length === 0){
    return true;
  } else {
    return false;
  }
};

HanoiGame.prototype.move = function(startTowerIdx, endTowerIdx){
  if(this.isValidMove(startTowerIdx, endTowerIdx)){
    var stackToBeMoved = this.stacks[startTowerIdx][0];
    this.stacks[endTowerIdx].unshift(stackToBeMoved);
    this.stacks[startTowerIdx].shift();
    return true;
  } else{
    return false;
  }
};

HanoiGame.prototype.print = function(){
  for(var i = 0; i < this.stacks.length; i++){
    console.log("Tower " + (i+1) + ": " + JSON.stringify(this.stacks[i]));
  }
};

HanoiGame.prototype.promptMove = function(callback){
  this.print();
  reader.question("Give the start tower", function(answer){
    var start = parseInt(answer - 1);
    reader.question("Give the end tower", function(answer){
      var end = parseInt(answer - 1);
      callback(start, end);
    });
  });
};

HanoiGame.prototype.run = function(completionCallback){
  var game = this;
  this.promptMove(function(start, end){
    var allowed_indices = [0,1,2];
    if(allowed_indices.indexOf(start) === -1 || allowed_indices.indexOf(end) === -1){
      console.log("These are not valid towers");
    } else if(game.move(start, end) === false ) {
      console.log("This is not a valid move");
    }
    if(game.isWon()){
      completionCallback();
      reader.close();
    } else{
      game.run(completionCallback);
    }
  });
};

var game = new HanoiGame();
game.run(function(){
  console.log("You win!");
})