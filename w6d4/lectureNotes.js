/* global $*/

$.Colorpicker = function (el) {
	this.$el = $(el);
	this.$el.on('click', function() {
		var $circle = $(event.currentTarget)
		$circle.css('background-color', randomColor());
		$circle.css('height', randomInt(100, 300) + 'px');
	}
	this.$el.con('transisitioned' function() {
		this.$el.css('height', randomInt(100, 300) + 'px');
	}.bind(this));
}

// method name on prototype is lowercase, actual class is capped
$.fn.colorpicker = function(){
	this.each(function() {
		// in the each method, this refers to the current element in the array
		new $.Colorpicker(this);
	});
}

function randomColor() {
	return 'rgb(' + randomInt(0, 255) + ',' + randomInt(0, 255) + ',' + 
		randomInt(0, 255) + ')';
}

function randomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}


// NOTES //
// event.target returns the smallest element selected
// event.currentTarger returns the largest non-window element selected