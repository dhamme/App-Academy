class ContactSharesController < ApplicationController

  def create
    @contact_share = ContactShare.new(share_params)
    if @contact_share.save
      render json: @contact_share
    else
      render @contact_shares.errors.full_messages, status: :unprocessable_entity
    end
  end

  def destroy
    @contact_share = ContactShare.find(params[:id])
    if @contact_share
      @contact_share.destroy
      render json: @contact_share
    else
      render text: "can't destroy nil contact"
    end
  end

  protected
  def share_params
    params[:contact_share].permit(:contact_id, :user_id)
  end
end
