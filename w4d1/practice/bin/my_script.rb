require 'debugger'
# bin/myscript.rb
require 'addressable/uri'
require 'rest-client'

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users/1'
).to_s

begin
response = RestClient.delete( url )

rescue RestClient::UnprocessableEntity => e
  puts e
  debugger
end

puts response