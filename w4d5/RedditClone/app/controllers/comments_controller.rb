class CommentsController < ApplicationController
  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.new(comment_params)
    
    @comment.author = current_user
    @comment.post_id ||= @comment.parent_comment.post_id 

    if @comment.save
      redirect_to post_url(@comment.post)
    else
      flash.now[:errors] = @comment.errors.full_messages
      render :new
    end
  end

  def show
    @comment = Comment.find(params[:id])
    @new_comment = Comment.new(
      author_id: @comment.author_id, 
      post_id: @comment.post_id,
      parent_comment_id: @comment.id
      )
  end

  def comment_params
    params.require(:comment).permit(:content, :post_id, :parent_comment_id)
  end
end
