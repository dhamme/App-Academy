# == Schema Information
#
# Table name: comments
#
#  id                :integer          not null, primary key
#  content           :text
#  author_id         :integer
#  post_id           :integer
#  created_at        :datetime
#  updated_at        :datetime
#  parent_comment_id :integer
#

class Comment < ActiveRecord::Base
  validates :author_id, :content, :post_id, presence: true
  
  belongs_to(
    :author,
    class_name: "User",
    foreign_key: :author_id,
    primary_key: :id
  )

  belongs_to(
  	:parent_comment,
  	class_name: 'Comment',
  	foreign_key: :parent_comment_id,
  	primary_key: :id
  )

  has_many(
  	:child_comments,
  	class_name: 'Comment',
  	foreign_key: :parent_comment_id,
  	primary_key: :id
  )
  
  belongs_to :post
  
  
end
