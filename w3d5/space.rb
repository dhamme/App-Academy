class SpaceBase
	extend Cargoize
	extend Thusterable
	extend Cannonize
	# extend modules to make class methods 
	# include modules to make instance methods

	def self.fleet
		@fleet ||= []
	end
	
	def initialize
		self.class.fleet << self
	end
end

class Dodo < SpaceBase
	attach_cannons :laser => "pew", :extinction => "weeping"
	mount_thruster :wing
	install_cargo_bays :beak, :small_instestine, :east, :gall_bladder
end

module Thusterable
	def mount_thruster thruster_name
		defind_method "set_#{thruster_name}" do |val|
			# strings must be prepended with @ in order to be used with instance_variable_get/set
			ivar_name = "@#{thruster_name}_thruster" # colon is not included when symbols are interpolated
			instance_variable_set(ivar_name, val)
			puts "thruster #{thruster_name} set to #{instance_variable_get(ivar_name)}%"
		end
	end
end

module Cannonize
	def attach_cannons(options)
		options.each do |name, sound|
			define_method("fire_#{name_cannon}") do
				puts sound * 3 + " goes the #{name} cannon"
			end
		end

		define_method "fire_all_cannons" do
			options.keys.each { |name| self.send("fire_#{name}_cannon") }
		end
	end
end

module Cargoize
	def install_cargo_bays *bay_names
			define_method "central_bay" do
				@central_bay ||= {}
			end

			bay_names.each do |bay_name|
				"store_in_#{bay_name}_bay" do |stuff|
					puts "storing #{stuff} in #{bay_name} bay"
					central_bay[bay_name] = stuff
				end
			end
			define_method  "unload_cargo" do
				cargo = @central_bay.keys
				@central_bay = {}
				cargo
			end
		end
	end
end

class HothianShipBase 
	extend Cargoize
	extend Thusterable
end