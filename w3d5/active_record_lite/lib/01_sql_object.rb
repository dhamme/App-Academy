require_relative 'db_connection'
require_relative '02_searchable.rb'
require 'active_support/inflector'

require 'byebug'

# NB: the attr_accessor we wrote in phase 0 is NOT used in the rest
# of this project. It was only a warm up.

class SQLObject

  def self.columns
    return @columns if @columns
    query = "SELECT * FROM #{self.table_name}"
    sql_results = DBConnection.execute2(query)
    @columns = sql_results.first.map(&:to_sym)
  end

  def self.finalize!
    c = columns
    c.each do |column|
      define_method("#{column}") do
        attributes[column]
      end

      define_method("#{column}=") do |val|
        attributes[column] = val
      end
    end
  end

  def self.table_name=(table_name)
    @table_name = table_name
  end

  def self.table_name
    if @table_name.nil?
      @table_name = self.to_s.tableize # why is this self instead of self.class?
    end
    @table_name
  end

  def self.all
    query = "SELECT #{table_name}.* FROM #{table_name}"
    results = DBConnection.execute(query)
    parse_all(results)
  end

  def self.parse_all(results)
    output_array = []
    results.each do |result|
      output_array << self.new(result)
    end
    output_array
  end

  def self.find(id)
    query = <<-SQL
    SELECT *
    FROM #{table_name}
    WHERE id = ?
    LIMIT 1
    SQL
    result = DBConnection.execute(query, id)
    parse_all(result).first
  end

  def initialize(params = {})
    params.each_pair do |attr_name, value|
      attr_name = attr_name.to_sym
      raise "unknown attribute '#{attr_name}'" unless self.class.columns.include?(attr_name)
      self.send("#{attr_name}=", value)
    end
  end

  def attributes
    @attributes ||= {}
  end

  def attribute_values
    self.class.columns.map{ |col| self.send(col) }
  end

  def insert
    col_names = self.class.columns.join(', ')
    question_marks = (['?'] * self.class.columns.length).join(', ')
    query = "INSERT INTO #{self.class.table_name} (#{col_names}) VALUES (#{question_marks})"
    DBConnection.execute(query, *self.attribute_values)
    self.id = DBConnection.last_insert_row_id
  end

  def update
    set_line = self.class.columns.map{ |attr_name| "#{attr_name} = ?" }.join(', ')
    query = "UPDATE #{self.class.table_name} SET #{set_line} WHERE id = ?"
    DBConnection.execute(query, *self.attribute_values, self.id)
  end

  def save
    if self.id.nil?
      insert
    else
      update
    end
  end
end
