require_relative '02_searchable'
require 'active_support/inflector'
require 'byebug'

# Phase IIIa
class AssocOptions
  attr_accessor(
    :foreign_key,
    :class_name,
    :primary_key,
    :name # I added this
  )

  def model_class
    class_name.constantize
  end

  def table_name
    model_class.table_name
  end
end

class BelongsToOptions < AssocOptions
  def initialize(name, options = {})
     o = {
      foreign_key: "#{name.to_s.singularize.underscore}_id".to_sym,
      primary_key: :id,
      :class_name => name.to_s.singularize.camelcase
      }.merge(options)

      @name = name
      @foreign_key = o[:foreign_key]
      @primary_key = o[:primary_key]
      @class_name = o[:class_name]
  end
end

class HasManyOptions < AssocOptions
  def initialize(name, self_class_name, options = {})
    o = {
      foreign_key: "#{self_class_name.underscore}_id".to_sym,
      primary_key: :id,
      :class_name => name.to_s.singularize.camelcase
      # why does hasmanyoptions offer a default class name? Because it's not self.class.
      }.merge(options)
      
      @name = name
      @foreign_key = o[:foreign_key]
      @primary_key = o[:primary_key]
      @class_name = o[:class_name]
  end
end

module Associatable
  # Phase IIIb
  def belongs_to(name, options = {})
    # options = BelongsToOptions.new(name, options)
    # define_method(name) do 
    #   foreign_key = self.send(options.foreign_key)
    #   # self instance has getter method for every column in the DB, including foreign key
    #   klass = options.model_class # the class that this object 'belongs to'
    #   # return the instance of class whose primary key matches this instance's foreign key
    #   klass.where({ options.primary_key => foreign_key}).first

    assoc_options[name] = BelongsToOptions.new(name, options)
    define_method(name) do 
      foreign_key = self.send(self.class.assoc_options[name].foreign_key)
      klass = self.class.assoc_options[name].model_class
      klass.where({ self.class.assoc_options[name].primary_key => foreign_key}).first
    end
  end

  def has_many(name, options = {})
    options = HasManyOptions.new(name, self.to_s, options)
    # why is the 2nd argument self instead of self.class? 
    define_method(name) do 
      primary_key = self.send(options.primary_key)
      klass = options.model_class 
      klass.where({ options.foreign_key => primary_key })
    end
  end

  def assoc_options
    # Wait to implement this in Phase IVa. Modify `belongs_to`, too.
      if @assoc_options.nil?
        @assoc_options = {}
      end
      @assoc_options
  end
end

class SQLObject
  extend Associatable # Mixin Associatable here...
end
