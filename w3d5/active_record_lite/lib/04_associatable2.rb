require_relative '03_associatable'
require 'byebug'

# Phase IV
module Associatable
  # Remember to go back to 03_associatable to write ::assoc_options

  def has_one_through(name, through_name, source_name)

  	# is there a reason I can't do this? 
    # define_method(name) do 
    # 	self.send(through_name).send(source_name)
    # end
    define_method(name) do
	    through_options = self.class.assoc_options[through_name]
	    # name: :human, foreign_key: :owner_id, primary_key: ;id, class_name: 'Human'
	    source_options = through_options.model_class.assoc_options[source_name]
	    # name: :house. foreign_key: :house_id, primary_key: :id

	    query = <<-SQL
	    SELECT 
	    	#{source_options.table_name}.* -- houses
	    FROM 
	    	#{source_options.table_name}  -- houses
	    JOIN 
	    	#{through_options.table_name} -- humans
	    ON 
	    	-- humans.house_id = houses.id
	    	#{through_options.table_name}.#{source_options.foreign_key} = 
	    	#{source_options.table_name}.#{source_options.primary_key}
	   
	    WHERE 
	    -- humans.id = ?
	    	#{through_options.table_name}.#{through_options.primary_key} = ?
	    SQL
	    result = DBConnection.execute(query, self.send(through_options.foreign_key))
	    source_options.model_class.parse_all(result).first
	  end
  end
end


