require_relative 'db_connection'
require_relative '01_sql_object'

module Searchable
  def where(params)
  	relation = Relation.new(self, params)
  	# where_line = params.keys.map{ |key| "#{key} = ?" }.join(' AND ')
   #  query = "SELECT * FROM #{self.table_name} WHERE #{where_line}"
   #  results = DBConnection.execute(query, *params.values)
   #  parse_all(results)
  end
end

class SQLObject
  extend Searchable
end


class Relation
	attr_reader :params
	def initialize(klass, params)
		@params = params
		@klass = klass
	end

	def where(params)
		@params.merge!(params)
		self
	end

	def to_a
		where_line = @params.keys.map{ |key| "#{key} = ?" }.join(' AND ')
    query = "SELECT * FROM #{@klass.send(table_name)} WHERE #{where_line}"
    results = DBConnection.execute(query, *@params.values)
    parse_all(results)
	end
end