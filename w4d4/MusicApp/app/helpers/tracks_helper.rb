module TracksHelper
	def ugly_lyrics(lyrics)
		output_string = ''
		lyrics.each_line do |line|
			output_string << ' &#9835; ' + line
		end
		output_string
	end
end
