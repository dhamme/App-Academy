class TracksController < ApplicationController

	before_action :require_login
	
	def show
		@track = Track.find(params[:id])
		render :show
	end

	def new
		@track = Track.new
		render :new
	end

	def create
		@track = Track.new(track_params)
		if @track.save!
			redirect_to track_url(@track)
		else
			render :new
		end
	end

	def edit
		@track = Track.find(params[:id])
		render :edit
	end

	def update
		@track = Track.find(params[:id])
		if @track.update(track_params)
			redirect_to track_url(@track)
		else
			flash.now[:errors] = @track.errors.full_messages
			render :edit
		end
	end

	def destroy
		track_to_destroy = Track.find(params[:id])
		album = track_to_destroy.album
		track_to_destroy.destroy
		redirect_to album_url(album)
	end

	protected
	def track_params
		params.require(:track).permit(:title, :album_id, :lyrics, :bonus, :track_number)
	end
end
