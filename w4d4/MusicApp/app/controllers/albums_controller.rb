class AlbumsController < ApplicationController
	before_action :require_login
	def show
		@album = Album.find(params[:id])
		render :show
	end

	def new
		@album = Album.new
		render :new
	end

	def create
		@album = Album.new(album_params)
		if @album.save!
			redirect_to album_url(@album)
		else
			render :new
		end
	end

	def edit
		@album = Album.find(params[:id])
		render :edit
	end

	def update
		@album = Album.find(params[:id])
		if @album.update(album_params)
			redirect_to album_url(@album)
		else
			flash.now[:errors] = @album.errors.full_messages
		end
	end

	def destroy
		album_to_destroy = Album.find(params[:id])
		band = album_to_destroy.band
		album_to_destroy.destroy
		redirect_to band_url(band)
	end

	protected
	def album_params
		params.require(:album).permit(:title, :band_id, :performance_type)
	end
end
