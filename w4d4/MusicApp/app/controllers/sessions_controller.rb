class SessionsController < ApplicationController
	
	def new
		@user = User.new
		render :new
	end

	def create
		user = User.find_by_credentials(
			params[:user][:email],
			params[:user][:password])

		if user
			log_user_in!(user)
			redirect_to user_url(user)
		else
			# why are we rendering here? Shouldn't this raise a flash error?
			render json: "credentials are incorrect"
		end
	end

	def destroy
		current_user.reset_session_token!
		session[:session_token] = nil
		redirect_to new_session_url
	end


end
