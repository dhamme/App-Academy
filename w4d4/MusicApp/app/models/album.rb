# == Schema Information
#
# Table name: albums
#
#  id               :integer          not null, primary key
#  title            :string(255)      not null
#  band_id          :integer          not null
#  performance_type :string(255)      not null
#  created_at       :datetime
#  updated_at       :datetime
#

class Album < ActiveRecord::Base
	PERFORMANCE_TYPES = %w(Live Studio)
	validates :title, :band_id, :performance_type, presence: true
	validates :performance_type, inclusion: { in: PERFORMANCE_TYPES ,
		message: "%{value} is not a valid performance type" }

	belongs_to(
		:band,
		class_name: 'Band',
		foreign_key: :band_id,
		primary_key: :id
	)

	has_many(
		:tracks,
		class_name: 'Track',
		foreign_key: :album_id,
		primary_key: :id,
		dependent: :destroy
	)
end
