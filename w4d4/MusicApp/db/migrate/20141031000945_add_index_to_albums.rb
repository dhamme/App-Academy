class AddIndexToAlbums < ActiveRecord::Migration
  def change
  	add_index :albums, [:title, :band_id], unique: true
  end
end
