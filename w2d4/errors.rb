class InvalidMoveError < StandardError
end

class InvalidSelectionError < StandardError
end