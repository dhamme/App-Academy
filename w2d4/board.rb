require_relative 'piece'
require_relative 'errors'
require 'byebug'
require 'colorize'

class Board
	attr_accessor :board

	def initialize(setup = true)
		@board = Array.new(8) { Array.new(8) }
		setup_board if setup
	end

	def [](pos)
		x,y = pos
		@board[x][y]
	end

	def []=(pos,piece)
		x,y = pos
		@board[x][y] = piece
	end

	def dup
    dup_board = Board.new(false)

    dup_board.board.each_with_index do |row, x|
      row.each_index do |y|
        piece = self[[x, y]]
        unless piece.nil?
        	dup_board[[x, y]] = Piece.new(dup_board, [x, y], piece.color)
        end
      end
    end

    dup_board
  end

	def setup_board
		0.upto(7) do |row_i|
			next if [3,4].include?(row_i)
			color = (row_i < 3 ? :black : :red)
			0.upto(7) do |col_i|
				piece_pos = [row_i, col_i]
				next unless valid_square?(piece_pos)
				self[piece_pos] = Piece.new(self, piece_pos, color)
			end
		end
	end

	def valid_square?(pos)
		row, col = pos
		(row.even? && col.odd?) || (row.odd? && col.even?)
	end

	def render
		@board.each_with_index do |row, row_i|
			row_output = row_i.to_s
			row.each_with_index do |el, col_i|
				sq_color = square_color(row_i, col_i)
				if el.nil? 
					row_output += '   '.colorize(background: sq_color)
				else
					row_output += (' ' + el.icon + ' ').colorize(background: sq_color)
					end
			end
			puts row_output + "\n"
		end
		puts '  ' + (0..7).to_a.join('  ')
		puts "======================="
	end

	def square_color(idx1, idx2)
    if (idx1.even? && idx2.even?) || (idx1.odd? && idx2.odd?)
      :light_red
    else
      :light_black
    end
  end

	def players_remaining_pieces(color)
		@board.flatten.compact.select{ |piece| piece.color == color }
	end
end

#if row is even and col is odd or if row is odd and col is even


# b = Board.new(false)
# b[[7,0]] = Piece.new(b,[7,0], :red)
# b[[6,1]] = Piece.new(b, [6,1], :black)
# b[[4,1]] = Piece.new(b, [4,1], :black)
# b.display
# b[[7,0]].perform_moves([[5,2],[3,0]])
# b.display
# b[[3,0]].perform_moves([[2,1]])
# b[[2,1]].perform_moves([[1,0]])
# b.display
# p b[[1,0]].promoted
# b[[1,0]].perform_moves([[0,1]])
# b.display


### performs slides and jumps ###
# b[[5,0]].perform_slide([4,1])
# b.display
# b[[2,3]].perform_slide([3,4])
# b.display
# b[[5,6]].perform_slide([4,7])
# b.display
# b[[2,1]].perform_slide([3,2])
# b.display
# b[[4,1]].perform_moves([[2,2]])
# b.display
###