require_relative 'board'
require_relative 'boardui'
require 'byebug'
require 'io/console'

class Game
	def initialize(name1, name2, board = Board.new(true))
		@player1 = name1
		@player2 = name2
		current_player = @player1
		@board = board
		@ui = BoardUI.new(@board)
	end

	# def play
	# 	byebug
	# 	puts "let's play a game!"
	# 	until over?
	# 		@board.render

	# 		puts "#{ @current_player } which piece would you like to move?"
	# 		start_pos = gets.chomp.split.map(&:to_i)
	# 		# check if it's valid
	# 		puts "where would you like to move it to?"
	# 		moves_arr = []
	# 		next_move = gets.chomp.split.map(&:to_i)
	# 		#check if it's valid
	# 		moves_arr << next_move
	# 		until next_move == ['n']
	# 			puts "any additional moves from there? n or enter the move"
	# 			next_move = gets.chomp.split
	# 			moves_arr << next_move unless next_move == ['n']
	# 		end
	# 		@board[start_pos].perform_moves(moves_arr)
	# 	end
	# end

	def play
		#until over?
		loop do
			take_turn
		end
	end

	def take_turn
    @ui.place_cursor([4, 4])
    all_moves = @ui.get_selection
    #board.validate_from(turn, from_square)
    #board.validate_to(turn, from_square)
    @board[all_moves.first].perform_moves(all_moves.drop(1))
    #board.move_piece!(move.first, move.last)
    @ui.clear_selection
  # rescue MoveError => e
  #   puts e.message
  #   retry
  end

	def over?
		@board.players_remaining_pieces(:red).count == 0 || 
		@board.players_remaining_pieces(:black).count == 0
	end

	def switch_turns
		@current_player = (current_player == @player1 ? @player2 : @player1 )
	end
end
b = Board.new(false)
b[[7,0]] = Piece.new(b,[7,0], :red)
b[[6,1]] = Piece.new(b, [6,1], :black)
b[[4,3]] = Piece.new(b, [4,3], :black)
b[[2,5]] = Piece.new(b, [2,5], :black)

g = Game.new('me', 'myself', b)
g.play

