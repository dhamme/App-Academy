require 'io/console'
require 'colorize'
require 'byebug'

class BoardUI
  
  attr_accessor :grid
  
  def initialize(board = nil)
    @board = board
    @cursor = [0,0]
    @grid = Array.new(8) { Array.new(8) }
    @selected = []
  end

  def render
    raise "No grid to display!" if @board.nil?

    rows = @board.board.map.with_index do |row, row_i|
      "#{row_i} " + row.map.with_index do |el, col_i|
        sq_color = square_color(row_i, col_i)
        if [row_i, col_i] == @cursor
          if el.nil? 
            '   '.colorize(background: :green)
          else
            (' ' + el.icon + ' ').colorize(background: :green)
          end
        else
          if el.nil? 
            if @selected.include?([row_i, col_i])
              '   '.colorize(background: :light_blue)
            else
              '   '.colorize(background: sq_color)
            end
          else
            if @selected.include?([row_i, col_i])
              '   '.colorize(background: :light_blue)
            else
              (' ' + el.icon + ' ').colorize(background: sq_color)
            end
          end
        end
        # if el.nil? 
        #   row_output += '   '.colorize(background: sq_color)
        # else
        #   row_output += (' ' + el.icon + ' ').colorize(background: sq_color)
        #   end
      end.join('')
    end.join("\n")
    puts '   ' + (0..7).to_a.join('  ')
    puts rows
    puts "======================="
  end

  def render_king(color)
    rows = @board.board.map.with_index do |row, row_i|
      "#{row_i} " + row.map.with_index do |el, col_i|
        loser_color = (color == :red ? :black : :red)
        sq_col = (king_pos?([row_i, col_i]) ? color : loser_color)
        '   '.colorize(background: sq_col)
        # if el.nil? 
        #   row_output += '   '.colorize(background: sq_color)
        # else
        #   row_output += (' ' + el.icon + ' ').colorize(background: sq_color)
        #   end
      end.join('')
    end.join("\n")
    puts '   ' + (0..7).to_a.join('  ')
    puts rows
    puts "======================="
  end

  def square_color(idx1, idx2)
    if (idx1.even? && idx2.even?) || (idx1.odd? && idx2.odd?)
      :light_red
    else
      :light_black
    end
  end

  def king_pos?(pos)
    [[0,5],[0,6],[0,7],[1,4],[1,5],[1,6],[2,3],[2,4],[2,5],[3,2],[3,3],[3,4],
    [0,0],[0,1],[1,0],[1,1],[2,0],[2,1],[3,0],[3,1],[4,0],[4,1],
    [5,0],[5,1],[6,0],[6,1],[7,0],[7,1],
    [4,2],[4,3],[4,4],[5,4],[5,5],[6,4],[6,5],[6,6],[7,5],[7,6],[7,7]
  ].include?(pos)
  end
  
  def get_selection(message=nil)
    current_message = message
    selection = nil
    until selection
      begin
        system("clear")
        render
        print(current_message) if current_message
        input = STDIN.getch
        selection = process_input(input)
        if @selected.include?([0,5])
          system("clear")
          render_king(:red) 
          sleep 2
        end
      rescue InputError => e
        current_message = e.message
        retry
      end
    end
    selection
  end
  
  def place_cursor(coord)
    @cursor = coord
  end
  
  def clear_selection
    @selected = []
  end
  
  def process_input(input)
    i, j = @cursor

    case input
    when "w"
      @cursor = [(i - 1) % 8, j]
    when "a"
      @cursor = [i, (j - 1) % 8]
    when "s"
      @cursor = [(i + 1) % 8, j]
    when "d"
      @cursor = [i, (j + 1)  % 8]
    when "m"
      return @selected
    when "\r"
      unless @selected.include?(@cursor)
        @selected << @cursor
      else
        return @selected
      end
    when "\u0003"
      fail "quitting gracefully"
    else
      raise InputError.new("Please use WASD to move and enter to select")
    end
    false
  end
end

class InputError < RuntimeError
end

# ui = BoardUI.new
# p ui.get_selection("Get a selection")