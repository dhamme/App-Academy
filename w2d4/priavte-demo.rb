class Cat 
	def initialize(name)
		@name = name
	end

	def meow
		puts "#{ name } says meow"
	end

	private
	attr_reader: name
end

c = Cat.new("gizmo")
c.meow

# this works by calling name,
# but would not work if we called self.name