require 'byebug'
class Piece
	attr_reader :color, :icon, :promoted

	def initialize(board, pos, color)
		@board 		= board
		@pos 			= pos
		@color 		= color
		@promoted = false
		@icon 		= icon
	end

	def perform_moves(moves_arr)
		perform_moves!(moves_arr) if valid_move_seq?(moves_arr)
		maybe_promote
	end

	def perform_moves!(moves_arr)
		if moves_arr.length == 1 
			if perform_slide(moves_arr.first)
			elsif perform_jump(moves_arr.first)
			else
				raise InvalidMoveError
			end
		else
			perform_moves!(moves_arr.take(1)) && perform_moves!(moves_arr.drop(1))
		end
		return true
	end

	def valid_move_seq?(moves_arr)
		duped_board = @board.dup
		begin
			duped_board[@pos].perform_moves!(moves_arr)
		rescue
			false
		end
		true
	end

	def perform_slide(end_pos)
		valid_slides = move_deltas.map{ |(x,y)| [@pos[0] + x, @pos[1] + y] }
		return false unless valid_slides.include?(end_pos) && @board[end_pos].nil?
		@board[@pos], @board[end_pos], @pos = nil, self, end_pos
		true
	end

	def perform_jump(end_pos)
		jump_sq = jumped_sq(end_pos)
		return false unless valid_jump?(jump_sq, end_pos)
		@board[end_pos], @board[@pos], @pos = self, nil, end_pos
		@board[jump_sq] = nil
		true
	end

	def valid_jump?(jump_sq, end_pos)
		possible_jumps = jump_deltas.map{ |(x,y)| [@pos[0] + x, @pos[1] + y] }
		possible_jumps.include?(end_pos) && 
		@board[end_pos].nil? &&
		@board[jump_sq].color != @color
	end

	def maybe_promote
		if (@color == :red && @pos[0] == 0) || (@color == :black && @pos[0] == 7)
			@promoted = true
		end
	end

	def jumped_sq(end_pos)
		[(end_pos[0] - @pos[0]) / 2 + @pos[0], (end_pos[1] - @pos[1]) / 2 + @pos[1]]
	end

	def move_deltas
		if @promoted
			[[-1, -1], [-1, 1], [1, -1], [1, 1]]
		else
			@color == :red ? [[-1, -1], [-1, 1]] : [[1, -1], [1, 1]]
		end
	end

	def jump_deltas
		move_deltas.map{ |(x,y)| [x * 2, y * 2] }
	end

	def icon
		if promoted
			@color == :red ? '◎' : '◉'
		else
			@color == :red ? '○' : '●'
		end
	end
end

