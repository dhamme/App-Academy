Pokedex.RootView.prototype.createPokemon = function (attrs, callback) {
    var pokemon = new Pokedex.Models.Pokemon(attrs);
    var that = this;
    pokemon.save({}, {
        success: function(savedPokemon){
            that.pokes.push(savedPokemon);
            that.addPokemonToList(savedPokemon);
            callback(savedPokemon);
        }
    })
}

Pokedex.RootView.prototype.submitPokemonForm = function (event) {
    event.preventDefault();
    var formData = $(event.currentTarget).serializeJSON();
    this.createPokemon(formData, this.renderPokemonDetail.bind(this));
};
