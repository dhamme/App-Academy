Pokedex.RootView.prototype.renderPokemonDetail = function (pokemon) {
    var $detail = $('<div>').addClass('detail');
    var $img = $('<img>').css("display","block");
    var attributes = pokemon.attributes;
    $img.attr("src", attributes.image_url);
    $detail.append($img);
    for(attr in attributes){
        if (attr !== 'image_url') { 
            $detail.append($('<p>').append(attributes[attr]));
        }
    }
    var $ul = $('<ul>').addClass('toys');
    
    pokemon.fetch({
        success: function(pokemon) {
            debugger
            pokemon.toys().each(function(toy){
                var $li = $('<li>');
                $li.append(toy.attributes.name);
                $ul.append($li);
            })
        }
    }
    )
    $detail.append($ul)
    this.$pokeDetail.html($detail);
};

Pokedex.RootView.prototype.selectPokemonFromList = function (event) {
    event.preventDefault();
    var pokemonId = $(event.currentTarget).data('id');
    var pokemon = this.pokes.get(pokemonId);
    this.renderPokemonDetail(pokemon); 
};
