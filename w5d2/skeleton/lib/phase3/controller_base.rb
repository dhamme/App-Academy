require_relative '../phase2/controller_base'
require 'active_support/core_ext'
require 'erb'
require 'active_support/inflector'

module Phase3
  class ControllerBase < Phase2::ControllerBase
    # use ERB and binding to evaluate templates
    # pass the rendered html to render_content
    def render(template_name)
    	controller_path = "views/#{self.class.to_s.underscore}"
    	tempate_contents = File.read("#{controller_path}/#{template_name}.html.erb")
    	erb_obj = ERB.new(tempate_contents)
    	evaluated_erb = erb_obj.result(binding)

    	render_content(evaluated_erb, "text/html")
    end
  end
end
