require 'uri'

module Phase5
  class Params
    def initialize(req, route_params = {})
        @params = {}
        parse_www_encoded_form(req.query_string) 
        parse_www_encoded_form(req.body)
        @params = @params.merge(route_params)
    end

    def [](key)
        @params[key]
    end

    def to_s
      @params.to_json.to_s
    end

    class AttributeNotFoundError < ArgumentError; end;

    private
    # argument format
    # user[address][street]=main&user[address][zip]=89436
    # should return
    # { "user" => { "address" => { "street" => "main", "zip" => "89436" } } }
    def parse_www_encoded_form(www_encoded_form)
        return if www_encoded_form.nil?
        pairs = URI::decode_www_form(www_encoded_form)
        
        pairs.each do |pair|
            keys = parse_key(pair.first)
            value = pair.last
            current = @params

            keys[0..-2].each do |key|
                current[key] ||= {}
                current = current[key]
            end
            current[keys.last] = value
        end
    end

    # 'user[address][street]' should return ['user', 'address', 'street']
    def parse_key(key)
        key.scan(/\w+/)
    end
  end
end
