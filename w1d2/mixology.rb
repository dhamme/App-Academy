def remix(ing)
  output = ing
  alcohols, mixers = seperate_beverages(ing)
  until verify?(output, ing)
    output = []
    mixers.shuffle!
    alcohols.each_with_index do |alcohol, idx|
      output << [alcohol, mixers[idx]]
    end
  end

  output
end

def verify?(output, ing)
  output.none?{ |x| ing.include?(x)}
end

def seperate_beverages(ing)
  alcohols = []
  mixers = []
  
  ing.each do |x|
    alcohols << x.first
    mixers << x.last
  end
  
  [alcohols, mixers]
end

# def get_new_mix(ing, mixers, alcohol)
#   mixers.shuffle!
#   last_mixer = mixers.last
#   while ing.include?([alcohol, last_mixer])
#     mixers.shuffle!
#     p alcohol
#     p mixers
#     puts "new loop"
#     last_mixer = mixers.last
#     # last_mixer = mixers.pop
#     # mixers.unshift(last_mixer)
#
#   end
#
#   [alcohol, mixers.pop]
# end

p remix([
  ["rum", "coke"],
  ["gin", "tonic"],
  ["scotch", "soda"],
  ["robatussin", "juice"]
])
# if !ing.include?([alcohol, mixer.last])
#   output << [alcohol, mixer.pop]
# end