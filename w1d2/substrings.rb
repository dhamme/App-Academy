def substrings(str)
  output = []
  
  (str.length).times do |i|
    (str.length-i).times do |j|
      # puts "i:#{i}, j:#{str.length-j-1}, #{str[i..str.length-j-1]}"
      output << str[i..str.length-j-1]
    end
  end
  output.sort
end
p substrings('123456')