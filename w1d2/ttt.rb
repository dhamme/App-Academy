class Board
  attr_accessor :board, :winning_positions
  
  def initialize
    @board = Hash[ (0..8).map { |x| [x, nil] } ]
    @winning_positions = [
      [0,1,2],[3,4,5],[6,7,8],
      [0,3,6],[1,4,7],[2,5,8],
      [0,4,8],[6,4,2]
      ]
  end
  
  def end?
    won? || tied?
  end
  
  def won?  #true if all values at any three winning indexes at are the same and not nil
    winning_positions.any?{ |x| board[x[0]] && (board[x[0]] == board[x[1]]) && (board[x[1]] == board[x[2]]) }
  end
  
  def tied?
    board.none? { |k,v| v == nil } && !won?
  end
  
  def winner
    if won?
      x_moves = board.values.count { |x| x == 'X' }
      y_moves = board.values.count { |x| x == 'O' }
      max = [x_moves, y_moves].max
      x_moves == max ? 'X' : 'O'
    else
      "no one"
    end
  end
  
  def empty?(pos)
    self.board[pos] ? false : true
  end
  
  def place_mark(pos, mark)
    if empty?(pos)
      self.board[pos] = mark
     return true
    end
    false
  end
  
end

class Game
  attr_accessor :board, :player1, :player2, :current_player
  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    @board = Board.new()
    @current_player = @player1
  end
  
  
  def play
    until board.end?
      print_board
      
      if board.place_mark(self.current_player.move, current_player.symbol)
        current_player == player1 ? self.current_player = player2 : self.current_player = player1
      else
        puts "make a valid move"
      end
    end
    puts "This game is over!, #{board.winner} wins"
  end
  
  def print_board
    board.board.values.each_with_index do |val, idx|
      [2,5,8].include?(idx) ? (puts "#{val||'_'}") : (print "#{val||'_'}")
    end
  end
end

class Player
  attr_accessor :symbol, :desired_move
  def initialize(symbol)
    @symbol = symbol
    @desired_move = nil
  end
end

class ComputerPlayer < Player
  def move
    self.desired_move = Random.rand(10)-1
  end
end

class HumanPlayer < Player
  def move
    puts "make your move"
    self.desired_move = gets.chomp.to_i
  end
end

ben = HumanPlayer.new("X")
cpu = ComputerPlayer.new("O")
new_game = Game.new(ben, cpu)
new_game.play