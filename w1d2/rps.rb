def rps(input)
  computer_move = moves.keys.sample
  if moves.keys.include?(input)
    "#{computer_move}, #{moves[input][computer_move]}"
  end
end

def moves
  { "Rock" => {
    "Paper" => "Lose",
    "Scissor" => "Win",
    "Rock" => "Draw"
    },
  
  "Paper" => {
    "Paper" => "Draw",
    "Scissor" => "Lose",
    "Rock" => "Win"
    },
  
  "Scissor" => {
    "Paper" => "Win",
    "Scissor" => "Draw",
    "Rock" => "Lose"
    } 
  }
end
puts rps("Scissor")
