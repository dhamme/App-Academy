def run
  puts "give me a file"
  filename = gets.chomp
  contents = File.readlines(filename)
  shuffled_contents = contents.shuffle
  File.open("#{filename}-shuffled.txt", "w") do |f|
    shuffled_contents.each{ |line| f.puts line }
  end
end

run