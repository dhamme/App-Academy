def super_print(string, options = {})
  defaults = {
    :times => 1,
    :upcase => false,
    :reverse => false
  }
  options = defaults.merge(options)
  output = string
  
  options.keys.each do |option|
    output = output * options[:times] if option == :times
    output = output.send(option) if options[option] == true
  end
  output
end

puts super_print("Hello", :times => 3, :reverse => true, :upcase => true)