def factors(num)
  output = []
  
  (1..Math.sqrt(num)).each do |x|
    if num % x == 0
      output << num/x
      output << x
    end
  end
  
  p output.sort
end

factors(493)