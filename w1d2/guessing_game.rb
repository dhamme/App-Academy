def play
  number = Random.rand(10)
  until compare(number,prompt) == "win"
  end
end

def prompt
  puts "Give us your best guess"
  gets.chomp.to_i
end

def compare(number, guess)
  if number > guess
    puts "Too low"
  elsif number < guess
    puts "Too high"
  else
    puts "You Win!"
    return 'win'
  end
end

play