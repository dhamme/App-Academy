class Student
  attr_accessor :first, :last, :courses, :course_load
  
  def initialize(first, last)
    @first = first
    @last = last
    @courses = []
    @course_load = Hash.new(0)
  end
  
  def name
    "#{ first } #{ last }"
  end
  
  def enroll(course)
    raise "schedule conflict!" if courses.any? { |existing_course| existing_course.conflicts_with?(course) }
    
    unless course.students.include?(self)
      self.courses << course
      course.students << self
      self.course_load[course.dept] += course.credits
    end
  end
end

class Course
  attr_accessor :name, :dept, :credits, :students, :days, :time, :schedule
  
  def initialize(name, dept, credits, days, time)
    @name = name
    @dept = dept
    @credits = credits
    @days = days
    @time = time
    @students = []
    @schedule = Hash[ days.map { |day| [day, time] } ]
  end
  
  def add_student(student)
    student.enroll(self)
  end  
  
  def conflicts_with?(course2)
    schedule.each_pair do |day, time|
      return true if course2.schedule[day] == time
    end
    false
  end
end

tom = Student.new("Tom", "Sawyer")
alice = Student.new("Alice", "James")

math_course = Course.new("Math", "Math Dept", 3, [:mon, :tues, :wen], 2)
eng_course = Course.new("English", "English Dept", 4, [:mon, :tues, :wen, :thur, :fri], 2)

# p tom.name
math_course.add_student(tom)
eng_course.add_student(tom)
#
# p math_course.students
# p tom.course_load
# p tom.courses