def bubble(arr)
  output_arr = arr
  loop do
    changed = false
    (0..output_arr.length-2).each do |idx|
      if output_arr[idx] > output_arr[idx+1]
        output_arr[idx], output_arr[idx+1] = output_arr[idx+1],output_arr[idx]
        changed = true
      end
    end
    return output_arr if changed == false
  end
end

puts bubble([6,4,8,2,1])